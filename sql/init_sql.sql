create database if not exists blog character set utf8mb4 collate utf8mb4_general_ci;
use blog;
create table blog
(
  id         integer primary key auto_increment,
  type_id    integer NOT NULL,
  title      varchar(32),
  created_at bigint  default 0,
  created_by integer default 0,
  updated_at bigint  default 0,
  updated_by integer default 0,
  status     tinyint default false,
  index type_index (type_id)
);
create table type
(
  id         integer primary key auto_increment,
  name       varchar(32),
  created_at bigint  default 0,
  created_by integer default 0,
  updated_at bigint  default 0,
  updated_by integer default 0,
  status     tinyint default false
);
create table blog_detail
(
  id         integer primary key auto_increment,
  blog_id    integer  not null,
  text       LONGTEXT not null,
  text_type  integer  not null,
  created_at bigint  default 0,
  created_by integer default 0,
  updated_at bigint  default 0,
  updated_by integer default 0,
  status     tinyint default false,
  index blog_id_idx (blog_id)
);
create table tag
(
  id         integer auto_increment primary key,
  name       varchar(12) not null,
  created_at bigint  default 0,
  created_by integer default 0,
  updated_at bigint  default 0,
  updated_by integer default 0,
  status     tinyint default false
);
create table blog_tag
(
  id         integer auto_increment primary key,
  blog_id    integer not null,
  tag_id     integer not null,
  created_at bigint  default 0,
  created_by integer default 0,
  updated_at bigint  default 0,
  updated_by integer default 0,
  status     tinyint default false,
  index blog_id_idx (blog_id),
  index tip_id (tag_id)
);
create table user
(
  id         integer auto_increment primary key,
  account    varchar(32) not null,
  password   varchar(32) not null,
  created_at bigint  default 0,
  created_by integer default 0,
  updated_at bigint  default 0,
  updated_by integer default 0,
  status     tinyint default false,
  unique index account_idx (account)
);



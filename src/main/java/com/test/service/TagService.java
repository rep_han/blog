package com.test.service;

import com.test.common.DataResult;
import com.test.common.Result;
import com.test.dto.TagRequest;
import com.test.dto.TagResponse;

import java.util.List;

public interface TagService {
    Result add(TagRequest tagRequest);

    DataResult<List<TagResponse>> all();

}

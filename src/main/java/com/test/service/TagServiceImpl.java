package com.test.service;

import com.test.common.DataResult;
import com.test.common.Result;
import com.test.dao.TagDao;
import com.test.dto.TagRequest;
import com.test.dto.TagResponse;
import com.test.entity.TagEntity;
import com.test.holder.AuthHolder;
import com.test.utils.EntityUtils;
import com.test.utils.ModelMapperUtils;
import com.test.utils.Results;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TagServiceImpl implements TagService {
    @Autowired
    private TagDao tagDao;

    @Autowired
    private AuthHolder authHolder;

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public Result add(TagRequest tagRequest) {
        TagEntity tagEntity = new TagEntity();
        tagEntity.setName(tagRequest.getName());
        EntityUtils.initBaseEntity(tagEntity, authHolder);
        tagDao.insert(tagEntity);
        return Results.success();
    }

    @Override
    public DataResult<List<TagResponse>> all() {
        List<TagEntity> all = tagDao.all();
        List<TagResponse> result = ModelMapperUtils.map(all,TagResponse.class);
        return Results.success(result);
    }
}

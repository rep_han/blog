package com.test.service;

import com.test.common.DataResult;
import com.test.dto.AccountRequest;

public interface AuthService {
    DataResult<Integer> auth(String token);

    DataResult<String> mockLogin();

    DataResult<String> signUp(AccountRequest accountRequest);

    DataResult<String> signIn(AccountRequest accountRequest);
}

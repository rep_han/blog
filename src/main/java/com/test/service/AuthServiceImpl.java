package com.test.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.test.common.DataResult;
import com.test.constant.ErrorCodeEnum;
import com.test.dao.UserDao;
import com.test.dto.AccountRequest;
import com.test.entity.UserEntity;
import com.test.properties.AuthConfig;
import com.test.utils.Results;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;
import java.util.Objects;
import java.util.Optional;

@Service
public class AuthServiceImpl implements AuthService {

    private final AuthConfig authConfig;

    private final static String EXP_KEY = "expAt";
    private final static String USER_ID_KEY = "user_id";

    private UserDao userDao;

    public AuthServiceImpl(AuthConfig authConfig, UserDao userDao) {
        this.authConfig = authConfig;
        this.userDao = userDao;
    }


    @Override
    public DataResult<Integer> auth(String token) {
        if (token == null) {
            return Results.failedData("403", "error token");
        }
        DecodedJWT decode;
        try {
            decode = JWT.decode(token);
        } catch (JWTDecodeException e) {
            return Results.failedData("403", "error token");
        }
        Claim claim = decode.getClaim(EXP_KEY);
        if (claim.isNull()) {
            return Results.failedData("403", "error token");
        }
        Long exp = claim.asLong();
        if (exp < System.currentTimeMillis()) {
            return Results.failedData("403", "login invalid");
        }
        Claim userIdClaim = decode.getClaim(USER_ID_KEY);
        Integer userId = userIdClaim.asInt();
        return Results.success(userId);
    }

    @Override
    public DataResult<String> mockLogin() {
        return Results.success(buildJWT(0));
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public DataResult<String> signUp(AccountRequest accountRequest) {
        UserEntity user = userDao.findByAccount(accountRequest.getAccount());
        if (user != null) {
            return Results.failedData(ErrorCodeEnum.CONFLICT_ACCOUNT);
        }
        UserEntity entity = new UserEntity();
        entity.setAccount(accountRequest.getAccount());
        entity.setPassword(accountRequest.getPassword());
        entity.setCreatedAt(System.currentTimeMillis());
        entity.setCreatedBy(0);
        entity.setUpdatedAt(System.currentTimeMillis());
        entity.setUpdatedBy(0);
        userDao.insert(entity);
        return Results.success(buildJWT(entity.getId()));
    }

    @Override
    public DataResult<String> signIn(AccountRequest accountRequest) {
        UserEntity entity = userDao.findByAccount(accountRequest.getAccount());
        Boolean valid = Optional.ofNullable(entity).map(t -> Objects.equals(t.getPassword(), accountRequest.getPassword())).orElse(false);
        if (!valid) {
            return Results.failedData(ErrorCodeEnum.ERROR_PASSWORD);
        }
        return Results.success(buildJWT(entity.getId()));
    }

    private String buildJWT(Integer userId) {
        try {
            return JWT
                    .create()
                    .withClaim(EXP_KEY, System.currentTimeMillis() + 3600 * 1000 * 24 * 30L)
                    .withClaim(USER_ID_KEY, userId)
                    .sign(Algorithm.HMAC512(authConfig.getAuthKey()));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

}

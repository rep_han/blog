package com.test.service;

import com.test.blog.BlogDetailFactory;
import com.test.common.DataResult;
import com.test.common.Result;
import com.test.constant.ErrorCodeEnum;
import com.test.dao.BlogDao;
import com.test.dao.BlogDetailDao;
import com.test.dao.BlogTagDao;
import com.test.dao.TagDao;
import com.test.dto.BlogDetailResponse;
import com.test.dto.BlogRequest;
import com.test.dto.BlogResponse;
import com.test.entity.BlogDetailEntity;
import com.test.entity.BlogEntity;
import com.test.entity.BlogTagEntity;
import com.test.holder.AuthHolder;
import com.test.utils.EntityUtils;
import com.test.utils.ModelMapperUtils;
import com.test.utils.Results;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class BlogServiceImpl implements BlogService {
    @Autowired
    private BlogDao blogDao;

    @Autowired
    private BlogDetailDao blogDetailDao;

    @Autowired
    private AuthHolder authHolder;

    @Autowired
    private BlogTagDao blogTagDao;

    @Autowired
    private TagDao tagDao;

    @Autowired
    private BlogDetailFactory blogDetailFactory;
    @Override
    @Transactional(rollbackFor = Throwable.class)
    public Result add(BlogRequest blogRequest) {
        BlogEntity blogEntity = new BlogEntity();
        blogEntity.setTitle(blogRequest.getTitle());
        blogEntity.setTypeId(blogRequest.getTypeId());
        EntityUtils.initBaseEntity(blogEntity, authHolder);
        blogDao.insert(blogEntity);

        BlogDetailEntity blogDetailEntity = new BlogDetailEntity();
        blogDetailEntity.setBlogId(blogEntity.getId());
        blogDetailEntity.setText(blogRequest.getText());
        EntityUtils.initBaseEntity(blogDetailEntity, authHolder);
        blogDetailDao.insert(blogDetailEntity);

        List<BlogTagEntity> blogTagEntities = blogRequest.getTagIds().stream().map(t -> {
            BlogTagEntity entity = ModelMapperUtils.map(t, BlogTagEntity.class);
            entity.setBlogId(blogEntity.getId());
            EntityUtils.initBaseEntity(entity, authHolder);
            return entity;
        }).collect(Collectors.toList());

        blogTagEntities.forEach(blogTagDao::insert);

        return Results.success();
    }

    @Override
    public DataResult<List<BlogResponse>> all() {
        List<BlogEntity> all = blogDao.all();
        List<BlogResponse> result = ModelMapperUtils.map(all,BlogResponse.class);
        return Results.success(result);
    }

    @Override
    public DataResult<List<BlogResponse>> searchByType(Integer typeId) {
        List<BlogEntity> search = blogDao.search(typeId);
        List<BlogResponse> result = ModelMapperUtils.map(search,BlogResponse.class);
        return Results.success(result);
    }

    @Override
    public DataResult<BlogDetailResponse> detail(Integer blogId) {
        BlogEntity blogEntity = blogDao.selectById(blogId);
        if (blogEntity == null) {
            return Results.failedData(ErrorCodeEnum.UNKNOWN_ID);
        }

        return blogDetailFactory.getBlogDetailResponse(blogId);
    }

    @Override
    public DataResult<List<BlogResponse>> searchByTag(Integer tagId) {
        List<BlogTagEntity> blogTagEntities = blogTagDao.findByTagId(tagId);
        Set<Integer> blogIds = blogTagEntities.stream().map(BlogTagEntity::getBlogId).collect(Collectors.toSet());
        List<BlogEntity> blogEntities = blogDao.findByIds(blogIds);
        List<BlogResponse> result = ModelMapperUtils.map(blogEntities,BlogResponse.class);
        return Results.success(result);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public Result update(Integer blogId, BlogRequest blogRequest) {
        BlogEntity blogEntity = blogDao.selectById(blogId);
        if (blogEntity == null) {
            return Results.failedData(ErrorCodeEnum.UNKNOWN_ID);
        }
        blogEntity.setTypeId(blogRequest.getTypeId());
        blogEntity.setTitle(blogRequest.getTitle());
        EntityUtils.updateBaseEntity(blogEntity, authHolder);
        blogDao.updateById(blogEntity);

        BlogDetailEntity blogDetailEntity = blogDetailDao.findByBlogId(blogId);
        blogDetailEntity.setText(blogRequest.getText());
        blogDetailEntity.setTextType(blogRequest.getTextType().value());
        EntityUtils.updateBaseEntity(blogDetailEntity, authHolder);
        blogDetailDao.updateById(blogDetailEntity);

        List<BlogTagEntity> blogTagEntities = blogTagDao.findByBlogId(blogId);
        Set<Integer> addTagIds = new HashSet<>();
        Map<Integer, BlogTagEntity> blogTagMap = blogTagEntities.stream().collect(Collectors.toMap(BlogTagEntity::getTagId, t -> t));
        for (Integer tagId : blogRequest.getTagIds()) {
            if (blogTagMap.containsKey(tagId)) {
                blogTagMap.remove(tagId);
            } else {
                addTagIds.add(tagId);
            }
        }
        Stream<BlogTagEntity> deleteStream = blogTagMap.values().stream().peek(t -> EntityUtils.deleteBaseEntity(t, authHolder));

        //new blog tag
        addTagIds.stream().map(t -> {
            BlogTagEntity result = new BlogTagEntity();
            result.setBlogId(blogId);
            result.setTagId(t);
            EntityUtils.initBaseEntity(result, authHolder);
            return result;
        }).forEach(blogTagDao::insert);

        //delete blog tag
        deleteStream.forEach(blogTagDao::updateById);


        return Results.success();
    }


}

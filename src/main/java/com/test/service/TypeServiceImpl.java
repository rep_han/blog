package com.test.service;

import com.test.common.DataResult;
import com.test.common.Result;
import com.test.dao.TypeDao;
import com.test.dto.TypeRequest;
import com.test.dto.TypeResponse;
import com.test.entity.TypeEntity;
import com.test.holder.AuthHolder;
import com.test.utils.EntityUtils;
import com.test.utils.ModelMapperUtils;
import com.test.utils.Results;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypeServiceImpl implements TypeService {
    @Autowired
    private TypeDao typeDao;

    @Autowired
    private AuthHolder authHolder;

    @Override
    public Result add(TypeRequest typeRequest) {
        TypeEntity entity = ModelMapperUtils.map(typeRequest, TypeEntity.class);
        EntityUtils.initBaseEntity(entity, authHolder);
        typeDao.insert(entity);
        return Results.success();
    }

    @Override
    public DataResult<List<TypeResponse>> all() {
        List<TypeEntity> all = typeDao.all();
        List<TypeResponse> result = ModelMapperUtils.map(all,TypeResponse.class);
        return Results.success(result);
    }
}

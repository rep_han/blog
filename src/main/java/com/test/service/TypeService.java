package com.test.service;

import com.test.common.DataResult;
import com.test.common.Result;
import com.test.dto.TypeRequest;
import com.test.dto.TypeResponse;

import java.util.List;

public interface TypeService {
    Result add(TypeRequest typeRequest);

    DataResult<List<TypeResponse>> all();
}

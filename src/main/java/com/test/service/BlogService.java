package com.test.service;

import com.test.common.DataResult;
import com.test.common.Result;
import com.test.dto.BlogDetailResponse;
import com.test.dto.BlogRequest;
import com.test.dto.BlogResponse;

import java.util.List;

public interface BlogService {
     Result add(BlogRequest testRequest);

     DataResult<List<BlogResponse>> all();

     DataResult<List<BlogResponse>> searchByType(Integer typeId);

     DataResult<BlogDetailResponse> detail(Integer blogId);

     DataResult<List<BlogResponse>> searchByTag(Integer tagId);

     Result update(Integer blogId, BlogRequest blogRequest);

}

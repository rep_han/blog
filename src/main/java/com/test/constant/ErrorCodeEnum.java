package com.test.constant;

import com.test.common.BaseErrorCode;
import lombok.Getter;

@Getter
public enum ErrorCodeEnum implements BaseErrorCode {
    CONFLICT_ACCOUNT("0001","account conflict"),
    ERROR_PASSWORD("0002","error password"),
    UNKNOWN_ID("0003", "unknown id"),
    ;
    private String code;
    private String msg;

    ErrorCodeEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}

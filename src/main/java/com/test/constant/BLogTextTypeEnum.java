package com.test.constant;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Objects;

public enum BLogTextTypeEnum {
    TEXT_TYPE(0),
    MD_TYPE(1),
    ;

    private final int typeId;

    BLogTextTypeEnum(int typeId) {
        this.typeId = typeId;
    }

    @JsonValue
    public int value() {
        return typeId;
    }

    @JsonFormat
    public static BLogTextTypeEnum valueOf(int typeId) {
        for (BLogTextTypeEnum value : values()) {
            if (Objects.equals(value.value(), typeId)) {
                return value;
            }
        }
        throw new IllegalArgumentException();
    }
}

package com.test.dao;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.test.entity.BaseEntity;
import com.test.entity.BlogEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Mapper
@Repository
public interface BlogDao extends BaseMapper<BlogEntity> {
    default List<BlogEntity> all() {
        return selectList(new LambdaQueryWrapper<>());
    }

    default List<BlogEntity> search(Integer typeId) {
        LambdaQueryWrapper<BlogEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(BlogEntity::getTypeId, typeId);
        return selectList(queryWrapper);
    }

    default List<BlogEntity> findByIds(Set<Integer> blogIds) {
        LambdaQueryWrapper<BlogEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(BlogEntity::getId, blogIds).eq(BaseEntity::isStatus, true);
        return selectList(queryWrapper);
    }
}

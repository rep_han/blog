package com.test.dao;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.test.entity.BaseEntity;
import com.test.entity.TagEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Mapper
@Repository
public interface TagDao extends BaseMapper<TagEntity> {
    default List<TagEntity> all() {
        LambdaQueryWrapper<TagEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(TagEntity::isStatus, true);
        return selectList(queryWrapper);
    }

    default List<TagEntity> findByIds(Collection<Integer> tagIds) {
        LambdaQueryWrapper<TagEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(TagEntity::getId, tagIds).eq(BaseEntity::isStatus, true);
        return selectList(queryWrapper);
    }
}

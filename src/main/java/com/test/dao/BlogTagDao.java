package com.test.dao;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.test.entity.BlogTagEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Mapper
@Repository
public interface BlogTagDao extends BaseMapper<BlogTagEntity> {

    default List<BlogTagEntity> findByBlogId(Integer blogId) {
        LambdaQueryWrapper<BlogTagEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(BlogTagEntity::getBlogId, blogId)
                .eq(BlogTagEntity::isStatus, true);
        return selectList(queryWrapper);
    }

    default List<BlogTagEntity> findByTagId(Integer tagId) {
        LambdaQueryWrapper<BlogTagEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(BlogTagEntity::getTagId, tagId)
                .eq(BlogTagEntity::isStatus, true);
        return selectList(queryWrapper);
    }

    default int removeByIds(Set<Integer> deleteIds) {
        LambdaQueryWrapper<BlogTagEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(BlogTagEntity::getId, deleteIds);
        return delete(wrapper);
    }
}

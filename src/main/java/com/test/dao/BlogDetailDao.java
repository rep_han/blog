package com.test.dao;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.test.entity.BlogDetailEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface BlogDetailDao extends BaseMapper<BlogDetailEntity> {
    default BlogDetailEntity findByBlogId(Integer blogId) {
        LambdaQueryWrapper<BlogDetailEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper
                .eq(BlogDetailEntity::getBlogId, blogId)
                .eq(BlogDetailEntity::isStatus, true)
        ;
        return selectOne(queryWrapper);
    }

}

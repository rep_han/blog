package com.test.dao;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.test.entity.BaseEntity;
import com.test.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface UserDao extends BaseMapper<UserEntity> {
    default UserEntity findByAccount(String account) {
        LambdaQueryWrapper<UserEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper
                .eq(UserEntity::getAccount, account)
                .eq(BaseEntity::isStatus, true);
        return selectOne(queryWrapper);
    }
}

package com.test.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.test.entity.TypeEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface TypeDao extends BaseMapper<TypeEntity> {
    default List<TypeEntity> all() {
        return selectList(new QueryWrapper<>());
    }
}

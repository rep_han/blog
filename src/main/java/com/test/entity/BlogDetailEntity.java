package com.test.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@TableName("blog_detail")
@Data
public class BlogDetailEntity extends BaseEntity {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer blogId;
    private String text;
    private int textType;
}

package com.test.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@TableName("user")
@Data
public class UserEntity extends BaseEntity{
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String account;
    private String password;
}

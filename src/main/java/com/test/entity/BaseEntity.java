package com.test.entity;

import lombok.Data;

@Data
public class BaseEntity {
    private int createdBy;
    private long createdAt;
    private int updatedBy;
    private long updatedAt;
    private boolean status;
}

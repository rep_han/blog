package com.test.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@TableName("tag")
@Data
public class TagEntity extends BaseEntity {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String name;
}

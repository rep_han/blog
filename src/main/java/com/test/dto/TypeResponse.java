package com.test.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel
public class TypeResponse {
    @ApiModelProperty("类型Id")
    private Integer id;
    @ApiModelProperty("类型名称")
    private String name;
}

package com.test.dto;

import com.test.constant.BLogTextTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@ApiModel
public class BlogRequest {
    @NotNull
    @NotBlank
    @ApiModelProperty("标题")
    private String title;

    @NotNull
    @ApiModelProperty("类型Id")
    private Integer typeId;

    @NotBlank
    @ApiModelProperty("正文")
    private String text;

    @NotNull
    @ApiModelProperty("正文类型")
    private BLogTextTypeEnum textType;

    @NotNull
    @ApiModelProperty("标签")
    private List<Integer> tagIds;
}

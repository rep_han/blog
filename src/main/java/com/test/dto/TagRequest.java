package com.test.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel
public class TagRequest {
    @NotBlank
    @ApiModelProperty("标签名称")
    private String name;
}

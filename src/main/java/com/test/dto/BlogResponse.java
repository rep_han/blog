package com.test.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel
public class BlogResponse {

    @ApiModelProperty("文章id")
    private Integer id;

    @ApiModelProperty("标题")
    private String title;

    @ApiModelProperty("创建时间")
    private Long createdAt;

    @ApiModelProperty("修改时间")
    private Long updatedAt;
}

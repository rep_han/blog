package com.test.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel
public class TypeRequest {

    @ApiModelProperty("类型名称")
    @NotBlank
    private String name;
}

package com.test.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel
public class TagResponse {

    @ApiModelProperty("标签id")
    private Integer id;

    @ApiModelProperty("标签名")
    private String name;
}

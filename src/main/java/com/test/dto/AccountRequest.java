package com.test.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@ApiModel
public class AccountRequest {
    @NotNull
    @Size(max = 32, min = 1)
    @ApiModelProperty("账号")
    private String account;
    @Size(max = 32, min = 8)
    @NotNull
    @ApiModelProperty("密码")
    private String password;
}

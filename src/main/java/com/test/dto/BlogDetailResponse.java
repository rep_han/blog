package com.test.dto;

import com.test.constant.BLogTextTypeEnum;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel
public class BlogDetailResponse extends BlogResponse {
    private TypeResponse type;
    private List<TagResponse> tags;
    private String text;
    private BLogTextTypeEnum textType;
}

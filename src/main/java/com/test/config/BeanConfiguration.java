package com.test.config;

import com.test.advice.AuthFilter;
import com.test.holder.AuthHolder;
import com.test.holder.NormalAuthHolder;
import com.test.service.AuthService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;

@Configuration
public class BeanConfiguration {
    @Bean
    public AuthHolder initAuthHolder(){
        return new NormalAuthHolder();
    }

    @Bean
    public Filter initAuthFilter(AuthHolder authHolder, AuthService authService) {
        return new AuthFilter((NormalAuthHolder) authHolder, authService);
    }
}

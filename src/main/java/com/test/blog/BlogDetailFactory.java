package com.test.blog;

import com.test.common.DataResult;
import com.test.dto.BlogDetailResponse;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class BlogDetailFactory implements ApplicationContextAware {


    private ApplicationContext applicationContext;

    public DataResult<BlogDetailResponse> getBlogDetailResponse(Integer blogId) {
        BlogDetailProduce<BlogDetailResponse> bean = applicationContext.getBean(BaseBlogDetailProduceImpl.class);
        return bean.buildBlogDetail(blogId);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}

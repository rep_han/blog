package com.test.blog;

import com.test.common.DataResult;
import com.test.dao.*;
import com.test.dto.BlogResponse;
import com.test.entity.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class BlogDetailProduceAbstract<T extends BlogResponse> implements BlogDetailProduce<T> {

    @Autowired
    private BlogDao blogDao;

    @Autowired
    private BlogDetailDao blogDetailDao;

    @Autowired
    private BlogTagDao blogTagDao;

    @Autowired
    private TypeDao typeDao;

    @Autowired
    private TagDao tagDao;

    @Override
    public abstract DataResult<T> buildBlogDetail(Integer blogId);


    protected TypeEntity getBlogType(Integer typeId) {
        return typeDao.selectById(typeId);
    }


    protected List<TagEntity> getBlogTag(Integer blogId) {
        List<BlogTagEntity> blogTagEntities = blogTagDao.findByBlogId(blogId);
        Set<Integer> tagIds = blogTagEntities.stream().map(BlogTagEntity::getTagId).collect(Collectors.toSet());
        return tagDao.findByIds(tagIds);
    }

    protected BlogEntity getBaseBlog(Integer blogId) {
        return blogDao.selectById(blogId);
    }

    protected BlogDetailEntity getBlogDetail(Integer blogId) {
        return blogDetailDao.findByBlogId(blogId);
    }
}

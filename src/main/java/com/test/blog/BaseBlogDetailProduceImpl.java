package com.test.blog;

import com.test.common.DataResult;
import com.test.constant.BLogTextTypeEnum;
import com.test.constant.ErrorCodeEnum;
import com.test.dto.BlogDetailResponse;
import com.test.dto.TagResponse;
import com.test.dto.TypeResponse;
import com.test.entity.BlogDetailEntity;
import com.test.entity.BlogEntity;
import com.test.entity.TagEntity;
import com.test.entity.TypeEntity;
import com.test.utils.ModelMapperUtils;
import com.test.utils.Results;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BaseBlogDetailProduceImpl extends BlogDetailProduceAbstract<BlogDetailResponse> {
    @Override
    public DataResult<BlogDetailResponse> buildBlogDetail(Integer blogId) {

        BlogEntity baseBlog = getBaseBlog(blogId);
        if (baseBlog == null) {
            return Results.failedData(ErrorCodeEnum.UNKNOWN_ID);
        }
        BlogDetailEntity blogDetail = getBlogDetail(blogId);
        List<TagEntity> blogTag = getBlogTag(blogId);
        TypeEntity blogType = getBlogType(baseBlog.getTypeId());
        BlogDetailResponse result = new BlogDetailResponse();
        result.setType(ModelMapperUtils.map(blogType, TypeResponse.class));
        result.setTags(ModelMapperUtils.map(blogTag, TagResponse.class));
        result.setText(blogDetail.getText());
        result.setTextType(BLogTextTypeEnum.valueOf(blogDetail.getTextType()));
        ModelMapperUtils.copy(baseBlog, result);
        return Results.success(result);
    }

}

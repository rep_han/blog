package com.test.blog;

import com.test.common.DataResult;
import com.test.dto.BlogResponse;

public interface BlogDetailProduce<T extends BlogResponse> {
    DataResult<T> buildBlogDetail(Integer blogId);
}

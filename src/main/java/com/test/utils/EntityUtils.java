package com.test.utils;

import com.test.entity.BaseEntity;
import com.test.holder.AuthHolder;

public class EntityUtils {
    public static void initBaseEntity(BaseEntity entity, AuthHolder authHolder) {
        entity.setCreatedAt(System.currentTimeMillis());
        entity.setCreatedBy(authHolder.getUserId());
        entity.setUpdatedAt(System.currentTimeMillis());
        entity.setUpdatedBy(authHolder.getUserId());
        entity.setStatus(true);
    }

    public static void updateBaseEntity(BaseEntity entity, AuthHolder authHolder) {
        entity.setUpdatedBy(authHolder.getUserId());
        entity.setUpdatedAt(System.currentTimeMillis());
    }

    public static void deleteBaseEntity(BaseEntity entity, AuthHolder authHolder) {
        updateBaseEntity(entity, authHolder);
        entity.setStatus(false);
    }
}

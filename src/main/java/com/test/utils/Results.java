package com.test.utils;

import com.test.common.BaseErrorCode;
import com.test.common.DataResult;
import com.test.common.Result;

public final class Results {
    public static Result success(){
        Result result = new Result();
        result.setSuccess(true);
        return result;
    }

    public static Result failed(String code, String msg){
        Result result = new Result();
        result.setSuccess(false);
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    public static Result failed(BaseErrorCode baseErrorCode) {
        return failed(baseErrorCode.getCode(), baseErrorCode.getMsg());
    }

    public static <T> DataResult<T> failedData(String code, String msg) {
        DataResult<T> result = new DataResult<>();
        result.setSuccess(false);
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    public static <T> DataResult<T> failedData(BaseErrorCode baseErrorCode) {
        return failedData(baseErrorCode.getCode(), baseErrorCode.getMsg());
    }

    public static <T> DataResult<T> success(T data) {
        DataResult<T> result = new DataResult<>();
        result.setData(data);
        result.setSuccess(true);
        return result;
    }
}

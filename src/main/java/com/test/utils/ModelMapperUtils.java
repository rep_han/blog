package com.test.utils;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import java.util.List;
import java.util.stream.Collectors;

public class ModelMapperUtils {
    private static final ModelMapper modelMapper = new ModelMapper();

    public static <T> T map(Object t, Class<T> tClass) {
        return modelMapper.map(t, tClass);
    }

    public static <T, R> List<T> map(List<R> rList,Class<T> tClass) {
        return rList.stream().map(t -> map(t, tClass)).collect(Collectors.toList());
    }

    public static void copy(Object source, Object target) {
        modelMapper.map(source, target);
    }
}

package com.test.controller;

import com.test.common.DataResult;
import com.test.common.Result;
import com.test.dto.TagRequest;
import com.test.dto.TagResponse;
import com.test.service.TagService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/tag")
public class TagController {
    @Autowired
    private TagService tagService;

    @PostMapping("")
    @ApiOperation("添加tag")
    public Result add(@RequestBody @Valid TagRequest tagRequest) {
        return tagService.add(tagRequest);
    }

    @GetMapping("")
    @ApiOperation("获取全部tag")
    public DataResult<List<TagResponse>> all() {
        return tagService.all();
    }

}

package com.test.controller;

import com.test.common.DataResult;
import com.test.common.Result;
import com.test.dto.BlogDetailResponse;
import com.test.dto.BlogRequest;
import com.test.dto.BlogResponse;
import com.test.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("blog")
public class BlogController {

    @Autowired
    private BlogService blogService;


    @PostMapping("")
    public Result add(@RequestBody @Valid BlogRequest blogRequest) {
        return blogService.add(blogRequest);
    }

    @GetMapping("/type/{typeId}")
    public DataResult<List<BlogResponse>> searchByType(@PathVariable Integer typeId) {
        return blogService.searchByType(typeId);
    }

    @GetMapping("")
    public DataResult<List<BlogResponse>> all(){
        return blogService.all();
    }

    @GetMapping("/{blogId}")
    public DataResult<BlogDetailResponse> detail(@PathVariable Integer blogId) {
        return blogService.detail(blogId);
    }

    @GetMapping("/tag/{tagId}")
    public DataResult<List<BlogResponse>> searchByTag(@PathVariable Integer tagId) {
        return blogService.searchByTag(tagId);
    }

    @PutMapping("/{blogId}")
    public Result update(@PathVariable Integer blogId, @RequestBody @Valid BlogRequest blogRequest) {
        return blogService.update(blogId, blogRequest);
    }

}

package com.test.controller;

import com.test.common.DataResult;
import com.test.dto.AccountRequest;
import com.test.service.AuthService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/auth")
public class AuthController {
    @Autowired
    private AuthService authService;

    @GetMapping("/mockLogin")
    @ApiOperation("生成模拟token")
    public DataResult<String> mockLogin() {
        return authService.mockLogin();
    }

    @PostMapping("/signUp")
    @ApiOperation("注册")
    public DataResult<String> signUp(@RequestBody @Valid AccountRequest accountRequest) {
        return authService.signUp(accountRequest);
    }

    @PostMapping("/signIn")
    @ApiOperation("登陆")
    public DataResult<String> signIn(@RequestBody @Valid AccountRequest accountRequest) {
        return authService.signIn(accountRequest);
    }
}

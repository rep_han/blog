package com.test.controller;

import com.test.common.DataResult;
import com.test.common.Result;
import com.test.dto.TypeRequest;
import com.test.dto.TypeResponse;
import com.test.service.TypeService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/type")
public class TypeController {
    @Autowired
    private TypeService typeService;

    @PostMapping("")
    @ApiOperation("添加类型")
    public Result add(@RequestBody TypeRequest type) {
        return typeService.add(type);
    }

    @GetMapping("")
    @ApiOperation("获取全部类型")
    public DataResult<List<TypeResponse>> all() {
        return typeService.all();
    }
}

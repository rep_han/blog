package com.test.common;

public interface BaseErrorCode {
    String getCode();

    String getMsg();
}

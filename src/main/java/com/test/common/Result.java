package com.test.common;

import lombok.Data;

@Data
public class Result {
    private boolean success;
    private String code;
    private String msg;
}

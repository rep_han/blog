package com.test.holder;

public class NormalAuthHolder implements AuthHolder {
    private ThreadLocal<Integer> authThreadLocal;

    public NormalAuthHolder(){
        authThreadLocal = new ThreadLocal<>();
    }

    @Override
    public Integer getUserId() {
        return authThreadLocal.get();
    }

    public void setUserId(Integer userId) {
        authThreadLocal.set(userId);
    }
}

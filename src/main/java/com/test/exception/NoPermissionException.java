package com.test.exception;

import lombok.Data;

@Data
public class NoPermissionException extends RuntimeException {
    private String msg;
    private String code = "403";

    public NoPermissionException(String msg) {
        super(msg);
        this.msg = msg;
    }
}

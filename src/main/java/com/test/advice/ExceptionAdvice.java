package com.test.advice;

import com.test.common.Result;
import com.test.exception.NoPermissionException;
import com.test.utils.Results;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;

@RestControllerAdvice
@Configuration
@Slf4j
public class ExceptionAdvice {


    @ExceptionHandler({NoPermissionException.class})
    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    public Result authExceptionIntercept(NoPermissionException exception) {
        log.error(exception.getMsg(), exception);
        return Results.failed("403", "auth error");
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public Result badRequest(Exception e) {
        if (e instanceof MethodArgumentNotValidException) {
            List<FieldError> fieldErrors = ((MethodArgumentNotValidException) e).getBindingResult().getFieldErrors();
            StringBuffer errorLog = new StringBuffer();
            fieldErrors.forEach(t -> {
                errorLog.append(t.getField()).append("|").append(t.getDefaultMessage());
            });
            log.error(errorLog.toString());
        }
        return Results.failed("400", "error param");
    }
    @ExceptionHandler({Exception.class})
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public Result exceptionIntercept(Throwable throwable) {
        log.error(throwable.getMessage(), throwable);
        return Results.failed("500", "unknownException");
    }
}

package com.test.advice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.common.DataResult;
import com.test.holder.NormalAuthHolder;
import com.test.service.AuthService;
import com.test.utils.Results;
import org.springframework.http.HttpStatus;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@WebFilter(filterName = "authFilter", urlPatterns = "/*")
public class AuthFilter implements Filter {

    private static final List<String> ALLOW_URLS = Arrays.asList(
            "auth",
            "swagger",
            "api-docs"
    );

    private NormalAuthHolder authHolder;

    private AuthService authService;

    public AuthFilter(NormalAuthHolder authHolder, AuthService authService) {
        this.authHolder = authHolder;
        this.authService = authService;
    }


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        String uri = httpServletRequest.getRequestURI();
        if (ALLOW_URLS.stream().anyMatch(uri::contains)) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }
        String token = httpServletRequest.getHeader("Authorization");
        DataResult<Integer> authResult = authService.auth(token);
        if (!authResult.isSuccess()) {
            quitMethod(servletResponse);
            return;
        }
        authHolder.setUserId(authResult.getData());
        filterChain.doFilter(servletRequest, servletResponse);
    }

    private void quitMethod(ServletResponse servletResponse) throws IOException {
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        try (ServletOutputStream outputStream = response.getOutputStream()) {
            byte[] errorMsg = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsBytes(Results.failed("403", "error user"));
            outputStream.write(errorMsg);
        }
    }

    @Override
    public void destroy() {

    }
}

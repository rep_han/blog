import com.google.gson.Gson;
import com.test.common.DataResult;
import com.test.dao.BlogDao;
import com.test.dao.BlogTagDao;
import com.test.dto.BlogResponse;
import com.test.entity.BlogEntity;
import com.test.entity.BlogTagEntity;
import com.test.service.BlogService;
import com.test.service.BlogServiceImpl;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.logging.log4j.util.Strings;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.Mockito.when;

/**
 * @author hanlipeng
 * @date 2019-04-19
 */

public class BlogServiceTest {
    @Mock
    BlogTagDao blogTagDao;
    @Mock
    BlogDao blogDao;

    @InjectMocks
    BlogService service = new BlogServiceImpl();

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mockTagDao();
        mockBlogDao();
    }

    private void mockBlogDao() {
        when(blogDao.findByIds(anySet())).then(t -> {
            Set<Integer> argument = t.getArgument(0);

            List<BlogEntity> result = argument.stream().map(id -> {
                BlogEntity blogEntity = new BlogEntity();
                blogEntity.setId(id);
                blogEntity.setTitle("any");
                blogEntity.setTypeId(RandomUtils.nextInt());
                return blogEntity;
            }).collect(Collectors.toList());

            return result;

        });

    }

    private void mockTagDao() {
        when(blogTagDao.findByTagId(anyInt())).then(t -> {
            Integer tagId = t.getArgument(0);
            LinkedList<BlogTagEntity> result = new LinkedList<>();
            for (int i = 0; i < 10; i++) {
                BlogTagEntity blogTagEntity = new BlogTagEntity();
                blogTagEntity.setBlogId(i);
                blogTagEntity.setId(i);
                blogTagEntity.setTagId(tagId);
                result.add(blogTagEntity);
            }
            return result;
        });
    }

    @Test
    public void searchByTagTest(){
        int tagId = 1;
        DataResult<List<BlogResponse>> result = service.searchByTag(tagId);
        Assert.assertTrue(result.isSuccess());
        List<BlogResponse> data = result.getData();
        Assert.assertNotNull(data);
        Assert.assertFalse(data.isEmpty());
        for (BlogResponse datum : data) {
            Assert.assertNotNull(datum);
            Assert.assertTrue(Strings.isNotBlank(datum.getTitle()));
        }
        System.out.println(new Gson().toJson(result));
    }
}
